

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php
            if(is_front_page()){
                echo wp_title("");
            } elseif(is_page()){ echo wp_title(); echo '- ';}
            elseif(is_search()){ echo 'Busca - ';}
            elseif(!(is_404()) && (is_single()) || (is_page())){
                echo wp_title(); echo '- ';
            } elseif(is_404()){echo 'nao encontrada - ';}
            bloginfo("name")
        ?>
    </title>
    <?php wp_head(); ?>
</head>

<body>
    <header id="top">

        <div id="topbar">
            <div id="topbar_logo">

                <figure>
                    <img id="logo_header" src="<?php echo get_stylesheet_directory_uri(); ?>/img/brafe.png" alt="Brafé Logo">
                </figure>
            </div>
            <div id="topbar_menu">
                <nav class="menuitem">
                <?php
                    $args = array(
                        'menu' => 'principal',
                        'theme_location' => 'navegacao',
                        'container' => false        
                    );
                    wp_nav_menu( $args );
                ?>
                </nav>
                <!-- <ul id="menu_item">
                    <li class="menuitem"><a href="/sobre">Sobre</a></li>
                    <li class="menuitem"><a href="/produtos">Produtos</a></li>
                    <li class="menuitem"><a href="/portfolio">Portfólio</a></li>
                    <li class="menuitem"><a href="/contato">Contato</a></li>
                </ul> -->
            </div>



        </div>
    </header>