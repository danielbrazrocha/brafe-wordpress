<footer>
<div id="newscontent">
                <div id="newsletter">

                    <div id="newsText">
                        <h2>Assine nossa newsletter</h2>
                        <h4>Promoções e eventos mensais</h4>
                    </div>
                    <div id="newssignin">

                        <input type="email" placeholder="Digite seu e-mail" />
                        <a href="/">
                            <div class="button">ENVIAR</div>
                        </a>
                    </div>
                </div>
            </div>
    <div id="footer_content">
                <div id="footer_box">
                    <div id="footer_text">
                        <p>
                            Este é um projeto da Origamid. Mais em origamid.com<br />
                            Praia de Botafoto, 300, 5º andar – Botafogo –Rio de Janeiro
                        </p>
                    </div id="footer_img">
                    <figure>
                        <img id="logo_footer" src="<?php echo get_stylesheet_directory_uri(); ?>/img/brafe.png" alt="Brafé Logo">
                    </figure>
                </div>
            </div>
    </footer>
    <?php wp_footer(); ?>


</body>


<script src="./index.js"></script>

</html>