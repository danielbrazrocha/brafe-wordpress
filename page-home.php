<?php 
// Template Name: Home Page
?>

<?php get_header(); ?>

    <section id="principal">
        <div id="intro">
            <h2><?php the_field('titulo_principal') ?></h2>
            <h3><?php the_field('subtitulo_do_texto_principal') ?></h3>
            
        </div>


        <div id="content">
            <div id="coffee_cards">
                <div>
                    <h3><?php the_field('subtitulo_carac_cafe') ?></h3>
                </div>
                <div id="card_list">
                    <div style="position: relative;">
                    
                        <div class="card_description"><?php the_field('prop1_cafe:texto') ?></div>
                        <figure>
                            <img id="logo_header" width="300" src="<?php the_field('prop1_cafe:img') ?>" alt="Brafé Logo">
                        </figure>
                    </div>
                    <div style="position: relative;">

                        <div class="card_description"><?php the_field('prop2_cafe:texto') ?></div>
                        <figure>
                            <img id="logo_header" width="300" src="<?php the_field('prop2_cafe:img') ?>" alt="Brafé Logo">
                        </figure>
                    </div>
                </div>
                <div>

                    <p><?php the_field('desc_carac_cafe') ?></p>
                </div>
            </div>


            <div id="coffee_types">

                <div id="coffee_type_cards">
                    <div class="coffee_type_card">
                        <div id="brown" class="circle"></div>
                        <h2><?php the_field('tipo_cafe1_id') ?></h2>
                        <p><?php the_field('tipo_cafe1_desc') ?></p>
                    </div>
                    <div class="coffee_type_card">
                        <div id="orange" class="circle"></div>
                        <h2><?php the_field('tipo_cafe2_id') ?></h2>
                        <p><?php the_field('tipo_cafe2_desc') ?></p>
                    </div>
                    <div class="coffee_type_card">
                        <div id="yellow" class="circle"></div>
                        <h2><?php the_field('tipo_cafe3_id') ?></h2>
                        <p><?php the_field('tipo_cafe3_desc') ?></p>
                    </div>
                </div>


                <a href="/">
                    <div class="button">SAIBA MAIS</div>
                </a>

            </div>

            <div id="stores">
                <div class="storecard">
                    <div class="storeimg">
                        <figure>
                            <img src="<?php the_field('foto_da_loja1') ?>" alt="Brafé Loja Botafogo">
                        </figure>
                    </div>
                    <div class="storedesc">
                        <h2><?php the_field('titulo_da_loja1') ?></h2>
                        <p><?php the_field('descricao_da_loja1') ?></p>
                        <a href="/">
                            <div class="button">VER MAPA</div>
                        </a>
                    </div>
                </div>
                <div class="storecard">
                    <div class="storeimg">
                        <figure>
                            <img src="<?php the_field('foto_da_loja2') ?>" alt="Brafé Loja Iguatemi">
                        </figure>
                    </div>
                    <div class="storedesc">
                        <h2><?php the_field('titulo_da_loja2') ?></h2>
                        <p><?php the_field('descricao_da_loja2') ?></p>
                        <a href="/">
                            <div class="button">VER MAPA</div>
                        </a>
                    </div>
                </div>
                <div class="storecard">
                    <div class="storeimg">
                        <figure>
                            <img src="<?php the_field('foto_da_loja3') ?>" alt="Brafé Loja Mineirao">
                        </figure>
                    </div>
                    <div class="storedesc">
                        <h2><?php the_field('titulo_da_loja3') ?></h2>
                        <p><?php the_field('descricao_da_loja3') ?></p>
                        <a href="/">
                            <div class="button">VER MAPA</div>
                        </a>
                    </div>
                </div>

            </div>

            

            








        </div>



    </section>

<?php get_footer(); ?>