<?php get_header(); ?>

    <section id="principal">
        <div id="intro">
            <h2> CAFÉS COM A CARA<br />DO BRASIL</h2>
            <h3>Direto das fazendas de Minas Gerais</h3>

        </div>


        <div id="content">
            <div id="coffee_cards">
                <div>
                    <h3>Uma Mistura de</h3>
                </div>
                <div id="card_list">
                    <div style="position: relative;">

                        <div class="card_description">amor</div>
                        <figure>
                            <img id="logo_header" width="300" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cafe-1.jpg" alt="Brafé Logo">
                        </figure>
                    </div>
                    <div style="position: relative;">

                        <div class="card_description">perfeicao</div>
                        <figure>
                            <img id="logo_header" width="300" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cafe-2.jpg" alt="Brafé Logo">
                        </figure>
                    </div>
                </div>
                <div>

                    <p>
                        O café é uma bebida produzida a partir dos grãos <br />
                        torrados do fruto do cafeeiro. É servido
                        tradicional<br />mente quente, mas também pode ser consumido gelado.<br />Ele é um estimulante,
                        por possuir
                        cafeína — geralmente<br /> 80 a 140 mg para cada 207 ml dependendo do método<br /> de
                        preparação.
                    </p>
                </div>
            </div>


            <div id="coffee_types">

                <div id="coffee_type_cards">
                    <div class="coffee_type_card">
                        <div id="brown" class="circle"></div>
                        <h2>Paulista</h2>
                        <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
                            tentou-se o
                            cultivo noutras regiões o João Alberto Castelo Branco trouxe mudas do Pará
                        </p>
                    </div>
                    <div class="coffee_type_card">
                        <div id="orange" class="circle"></div>
                        <h2>Carioca</h2>
                        <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
                            tentou-se o
                            cultivo noutras regiões o João Alberto Castelo Branco trouxe mudas do Pará</p>

                    </div>
                    <div class="coffee_type_card">
                        <div id="yellow" class="circle"></div>
                        <h2>Mineiro</h2>
                        <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
                            tentou-se o
                            cultivo noutras regiões o João Alberto Castelo Branco trouxe mudas do Pará</p>

                    </div>


                </div>


                <a href="/">
                    <div class="button">SAIBA MAIS</div>
                </a>

            </div>

            <div id="stores">
                <div class="storecard">
                    <div class="storeimg">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/botafogo.jpg" alt="Brafé Loja Botafogo">
                        </figure>
                    </div>
                    <div class="storedesc">
                        <h2>Botafogo</h2>
                        <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
                            tentou-se o cultivo</p>
                        <a href="/">
                            <div class="button">VER MAPA</div>
                        </a>
                    </div>
                </div>
                <div class="storecard">
                    <div class="storeimg">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/iguatemi.jpg" alt="Brafé Loja Iguatemi">
                        </figure>
                    </div>
                    <div class="storedesc">
                        <h2>Iguatemi</h2>
                        <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
                            tentou-se o cultivo</p>
                        <a href="/">
                            <div class="button">VER MAPA</div>
                        </a>
                    </div>
                </div>
                <div class="storecard">
                    <div class="storeimg">
                        <figure>
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mineirao.jpg" alt="Brafé Loja Mineirao">
                        </figure>
                    </div>
                    <div class="storedesc">
                        <h2>Mineirão</h2>
                        <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
                            tentou-se o cultivo</p>
                        <a href="/">
                            <div class="button">VER MAPA</div>
                        </a>
                    </div>
                </div>

            </div>

            <div id="newscontent">
                <div id="newsletter">

                    <div id="newsText">
                        <h2>Assine nossa newsletter</h2>
                        <h4>Promoções e eventos mensais</h4>
                    </div>
                    <div id="newssignin">

                        <input type="email" placeholder="Digite seu e-mail" />
                        <a href="/">
                            <div class="button">ENVIAR</div>
                        </a>
                    </div>
                </div>
            </div>

            








        </div>



    </section>

<?php get_footer(); ?>